﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    public float speed;
    public float rotSpeed;
    public float gravity = 0.5f;

    public bool onGround { private set; get; }
    private CharacterController cCtrl;
    private Rigidbody rBody;

    void Start()
    {
        rBody = GetComponent<Rigidbody>();
        cCtrl = GetComponent<CharacterController>();
    }

    void Update()
    {
        
    }
    
    public void Move(float vertical, float horizontal)
    {
        transform.Translate(new Vector3(0, 0, vertical * speed * Time.fixedDeltaTime));
    }

    public void Move(Vector3 position)
    {
        transform.position = position;
    }

    public void Rotate(float rotY)
    {
        transform.eulerAngles = new Vector3(transform.eulerAngles.x, rotY, transform.eulerAngles.z);
    }

    public void Rotate(Vector3 rotation)
    {
        transform.eulerAngles = rotation;
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Floor")
        {
            onGround = true;
        }
    }

    private void OnCollisionExit(Collision collision)
    {
        if (collision.gameObject.tag == "Floor")
        {
            onGround = false;
        }
    }
}
