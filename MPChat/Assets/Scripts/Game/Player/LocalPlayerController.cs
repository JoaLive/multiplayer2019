﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LocalPlayerController : MonoBehaviour
{
    private float vertical;
    private float horizontal;

    private PlayerMovement pMove;
    private LocalPlayerNet pNet;
    private Camera playerCamera;
    private Weapon weapon;

    void Start()
    {
        pMove = GetComponent<PlayerMovement>();
        pNet = GetComponent<LocalPlayerNet>();
        playerCamera = GameObject.FindGameObjectWithTag("PlayerCamera").GetComponent<Camera>();   
        weapon = GetComponentInChildren<Weapon>();
        playerCamera.GetComponentInParent<MouseAimCamera>().Initialize();
    }

    void Update() {
        vertical = Input.GetAxis("Vertical");
        horizontal = Input.GetAxis("Horizontal");

        pMove.Rotate(playerCamera.transform.eulerAngles.y);
        CheckInput();
    }
    void FixedUpdate()
    {
        pMove.Move(vertical, horizontal);
    }

    void CheckInput()
    {
        if (Input.GetMouseButtonUp(0))
        {
            weapon.Shoot();
            pNet.Shoot();
        }
    }

    public Vector2 GetInput() 
    {
        return new Vector2(horizontal, vertical);
    }
}
