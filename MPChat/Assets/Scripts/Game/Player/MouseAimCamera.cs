﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseAimCamera : MonoBehaviour
{
    public float cameraSpeed = 120.0f;
    public float inputSensitivity = 150.0f;
    public float clampAngle = 80.0f;
    public float camXtoPlayer;
    public float camYtoPlayer;
    public float camZtoPlayer;

    //public Transform cross;
    
    GameObject cameraObj;
    GameObject playerObj;
    GameObject cameraFollowObj;
    float mouseX;
    float mouseY;
    float smoothX;
    float smoothY;
    float rotY;
    float rotX;
    Vector3 follorPos;
    Vector3 hitPos;

    void Start()
    {
        Vector3 rot = transform.localRotation.eulerAngles;
        rotY = rot.y;
        rotX = rot.x;
 
    }

    void Update() 
    {
        if (playerObj == null)
            return;

        mouseX = Input.GetAxis("Mouse X");
        mouseY = Input.GetAxis("Mouse Y");

        rotY += mouseX * inputSensitivity * Time.deltaTime;
        rotX += -mouseY * inputSensitivity * Time.deltaTime;

        rotX = Mathf.Clamp(rotX, -clampAngle, clampAngle);

        Quaternion localRotation = Quaternion.Euler(rotX, rotY, 0);
        transform.rotation = localRotation;

        RaycastHit hit;
        if (Physics.Raycast(cameraObj.transform.position, cameraObj.transform.forward, out hit, 100))
        {
            Debug.DrawRay(cameraObj.transform.position, cameraObj.transform.TransformDirection(Vector3.forward) * hit.distance, Color.yellow);
            //transform.LookAt(hit.point);
            hitPos = hit.point;
        }
        else 
            hitPos = transform.position + cameraObj.transform.forward * 100; 
    }

    void LateUpdate()
    {
        if (playerObj == null)
            return;

        Transform target = cameraFollowObj.transform;

        float step = cameraSpeed * Time.deltaTime;
        Vector3 newPosition = new Vector3(target.position.x + camXtoPlayer, target.position.y + camYtoPlayer, target.position.z + camZtoPlayer);
        transform.position = Vector3.MoveTowards(transform.position, target.position, step);
    }
    
    public void Initialize() 
    {
        cameraObj = GameObject.FindGameObjectWithTag("PlayerCamera");   
        playerObj = GameObject.FindGameObjectWithTag("LocalPlayer");
        cameraFollowObj = GameObject.FindGameObjectWithTag("CameraPivot");


        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
    }

    public Vector3 GetHitPoint()
    {
        return hitPos;
    }
}
