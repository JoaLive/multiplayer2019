﻿using System;
using System.Net;
using System.Net.Sockets;
using UnityEngine;
using UnityEngine.UI;

public class ConectionScreen : MonoBehaviour
{
    public InputField iptPort;
    public InputField iptIP;
    public InputField iptName;

    public void StartServer()
    {
        try
        {
            NetworkManager.Instance.StartServer(int.Parse(iptPort.text));
            GameNetworkManager.Instance.isServer = true;
            GameNetworkManager.Instance.GameStarted();
            gameObject.SetActive(false);
        }
        catch (Exception ex)
        {
            Debug.LogError("No se pudo iniciar el server. ERROR: " + ex.Message);
        }
    }

    public void StartClient()
    {
        try
        {
            IPAddress ip;
            if (iptIP.text == "localhost")
                ip = LocalIPAddress();
            else
                ip = IPAddress.Parse(iptIP.text);
            
            NetworkManager.Instance.StartClient(ip, int.Parse(iptPort.text));
            GameNetworkManager.Instance.GameStarted();
            gameObject.SetActive(false);

            GameManager.Instance.localName = iptName.text;
        }
        catch (Exception ex)
        {
            Debug.LogError("No se pudo iniciar el server. ERROR: " + ex.Message);
        }
    }

    IPAddress LocalIPAddress()
    {
        IPHostEntry host;
        host = Dns.GetHostEntry(Dns.GetHostName());
        foreach (IPAddress ip in host.AddressList)
        {
            if (ip.AddressFamily == AddressFamily.InterNetwork)
            {
                return ip;
            }
        }
        return null;
    }
}
