﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Target : MonoBehaviour
{
    public bool active;
    public uint id;

    public void Activate()
    {
        if (active)
            return;

        active = true;
        transform.position = new Vector3(transform.position.x, transform.position.y + 100, transform.position.z + 100);
    }

    public void Deactivate()
    {
        if (!active)
            return;

        active = false;
        transform.position = new Vector3(transform.position.x, transform.position.y - 100, transform.position.z - 100);
    }

    private void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.tag == "Bullet" || active)
        {
            GameManager.Instance.TargetDown(other.gameObject.GetComponent<Bullet>().playerId, this);
        }
    }
}
