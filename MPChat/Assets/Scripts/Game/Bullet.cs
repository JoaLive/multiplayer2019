﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    public float force = 100;

    public ulong playerId;

    void Start()
    {
        Rigidbody rBody = GetComponent<Rigidbody>();
        rBody.AddForce(transform.forward * force);
    }

    private void OnCollisionEnter(Collision other) {
        if (other.gameObject.tag == "Floor")
            gameObject.SetActive(false);
    }
}
