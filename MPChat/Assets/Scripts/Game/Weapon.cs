﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Weapon : MonoBehaviour
{
    public GameObject playerCamera;
    public GameObject bullet;

    private void OnEnable()
    {
        if (!GameNetworkManager.Instance.isServer)
            playerCamera = GameObject.FindGameObjectWithTag("PlayerCamera");
    }

    void Update()
    {
        if (playerCamera != null)
            transform.eulerAngles = new Vector3(playerCamera.transform.eulerAngles.x, transform.eulerAngles.y, transform.eulerAngles.z);
    }

    public void Rotate(float x)
    {
        transform.eulerAngles = new Vector3(x, transform.eulerAngles.y, transform.eulerAngles.z);
    }

    public void Shoot()
    {
        GameObject goBullet = Instantiate(bullet, transform.position, transform.rotation);

        goBullet.GetComponent<Bullet>().playerId = GetComponentInParent<NetworkObject>().clientId;
    }
}
