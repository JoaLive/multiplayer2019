﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClientFrame
{
    public uint number;
    public float horizontal;
    public float vertical;
    public Vector3 position;
    public Vector3 rotation;

    public ClientFrame(uint number, float horizontal, float vertical, Vector3 position, Vector3 rotation)
    {
        this.number = number;
        this.horizontal = horizontal;
        this.vertical = vertical;
        this.position = position;
        this.rotation = rotation;
    }
}

public class ServerFrame
{
    public uint number;
    public Vector3 position;
    public Vector3 rotation;

    public ServerFrame(uint number, Vector3 position, Vector3 rotation)
    {
        this.number = number;
        this.position = position;
        this.rotation = rotation;
    }
}

public class ClientNetworkFrames : Singleton<ClientNetworkFrames>
{
    public uint frameNumber = 1;
    public Queue<ClientFrame> localFrames = new Queue<ClientFrame>();

    protected override void Initialize()
    {
        base.Initialize();
    }

    public void AddFrame(float horizontal, float vertical, Vector3 position, Vector3 rotation)
    {
        //Debug.Log("Guardo frame: " + frameNumber + " Position: " + position);
        localFrames.Enqueue(new ClientFrame(frameNumber, horizontal, vertical, position, rotation));
        frameNumber++;
    }

    public void SetFrameNumber(uint frameNumber, DateTime timeStamp)
    {
        double diff = (DateTime.Now - timeStamp).TotalSeconds;
        int frames = (int)Math.Round((diff / Time.fixedDeltaTime));
        this.frameNumber = (uint)frames + frameNumber;
    }

    public uint GetFrameNumber()
    {
        return frameNumber;
    }
}

public class ServerNetworkFrames : Singleton<ServerNetworkFrames>
{
    Dictionary<ulong, Queue<ServerFrame>> clientsFrames = new Dictionary<ulong, Queue<ServerFrame>>();

    uint frameNumber = 1;

    protected override void Initialize()
    {
        base.Initialize();
    }

    public void AddFrame(ulong clientId, uint frame, Vector3 position, Vector3 rotation)
    {
        if (!clientsFrames.ContainsKey(clientId))
            clientsFrames.Add(clientId, new Queue<ServerFrame>());

        clientsFrames[clientId].Enqueue(new ServerFrame(frame, position, rotation));
    }

    public uint GetCurrentFrame()
    {
        return frameNumber;
    }
    
    public ServerFrame GetCurrentFrameOfClient(ulong clientId)
    {
        if (clientsFrames.ContainsKey(clientId))
        {
            if (clientsFrames[clientId].Count > 0)
                return clientsFrames[clientId].Dequeue();
            else
                return null;
        }
        else
            return null;
    }
}
