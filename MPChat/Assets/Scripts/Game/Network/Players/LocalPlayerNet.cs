﻿using UnityEngine;
using System;
using System.Net;
using System.IO;

public class LocalPlayerNet : NetworkObject
{
    Vector3 lastPositionSent = new Vector3();
    Vector3 lastRotationSent = new Vector3();
    Vector3 lastScaleSent = new Vector3();

    PlayerMovement pMove;
    LocalPlayerController playerController;
    GameObject mainCamera; 

    void FixedUpdate()
    {
        Vector2 input = playerController.GetInput();
    }

    void Start()
    {
        pMove = GetComponent<PlayerMovement>();
        playerController = GetComponent<LocalPlayerController>();
        mainCamera = GameObject.FindGameObjectWithTag("PlayerCamera");
    }

    public void MovePlayer(float horizontal, float vertical)
    {
        pMove.Move(vertical, horizontal);
    }

    public PlayerTransformPacket GetNewTransform()
    {
        PlayerTransformPacket packet = new PlayerTransformPacket();
        
        if (transform.position == lastPositionSent &&
            transform.eulerAngles == lastRotationSent &&
            transform.localScale == lastScaleSent)
            return null;

        packet.payload = new PlayerTransform();

        packet.payload.hasPosition = false;
        packet.payload.hasRotation = false;
        packet.payload.hasScale = false;

        if (transform.position != lastPositionSent)      
        {       
            packet.payload.hasPosition = true;
            packet.payload.xPos = transform.position.x;
            packet.payload.yPos = transform.position.y;
            packet.payload.zPos = transform.position.z;
            lastPositionSent = transform.position;
        } 

        if (transform.eulerAngles != lastRotationSent)      
        {       
            packet.payload.hasRotation = true;
            packet.payload.xRot = transform.eulerAngles.x;
            packet.payload.yRot = transform.eulerAngles.y;
            packet.payload.zRot = transform.eulerAngles.z;
            lastRotationSent = transform.eulerAngles;
        }

        if (transform.localScale != lastScaleSent)      
        {       
            packet.payload.hasScale = true;
            packet.payload.xSca = transform.localScale.x;
            packet.payload.ySca = transform.localScale.y;
            packet.payload.zSca = transform.localScale.z;
            lastScaleSent = transform.localScale;
        } 

        return packet;
    }

    public PlayerInputPacket GetNewInput()
    {
        PlayerInputPacket packet = new PlayerInputPacket();

        Vector2 input = playerController.GetInput();
        
        packet.payload = new PlayerInput();

        packet.payload.horizontal = input.x;
        packet.payload.vertical = input.y;

        packet.payload.camHorizontal = Input.GetAxis("Mouse X");
        packet.payload.camVertical = Input.GetAxis("Mouse Y");
        packet.payload.xCamRot = mainCamera.transform.eulerAngles.x;

        packet.payload.xRot = transform.localEulerAngles.x;
        packet.payload.yRot = transform.localEulerAngles.y;
        packet.payload.zRot = transform.localEulerAngles.z;

        return packet;
    }

    public void Shoot()
    {
        GameNetworkManager.Instance.SendPlayerEvent("Shoot", objectId, clientId, ClientNetworkFrames.Instance.frameNumber);
    }

    void OnRecieve(ushort type, Stream stream, IPEndPoint ip)
    {
        switch (type)
        {
            case (ushort)UserPacketType.PlayerTransform:
                ProcessTransform(stream);
                break;
            default:
                break;
        }
    }

    void ProcessTransform(Stream stream)
    {
        PlayerTransformPacket packet = new PlayerTransformPacket();
        packet.Deserialize(stream);

        if (packet.payload.hasPosition)
            pMove.Move(new Vector3(packet.payload.xPos, packet.payload.yPos, packet.payload.zPos));

        if (packet.payload.hasRotation)
            pMove.Rotate(new Vector3(packet.payload.xRot, packet.payload.yRot, packet.payload.zRot));

    }

}
