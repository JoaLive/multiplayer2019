﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NetworkPlayerNet : NetworkObject
{
    public float speed = 25;
    public float mouseSensitivity = 150.0f;
    public float clampAngle = 80.0f;


    private Weapon weapon;
    CharacterController cCtrl;

    Vector3 lastPosition = Vector3.zero;
    Vector3 newPosition = Vector3.zero;
    Quaternion newRotation = Quaternion.identity;

    float vertical = 0;
    float horizontal = 0;
    float camVertical = 0;
    float camHorizontal = 0;

    Vector3 lastPositionSent = new Vector3();
    Vector3 lastRotationSent = new Vector3();
    Vector3 lastScaleSent = new Vector3();

    public uint frame = 0;
    uint lastFrameSaved = 0;

    private void Start()
    {
        cCtrl = GetComponent<CharacterController>();
        weapon = GetComponentInChildren<Weapon>();
        newPosition = transform.position;

    }

    private void FixedUpdate()
    {
        if (NetworkManager.Instance.isServer)
        {
            transform.Translate(new Vector3(0, 0, vertical * speed * Time.fixedDeltaTime));

            //float rotY = transform.localRotation.eulerAngles.y + (camHorizontal * mouseSensitivity * Time.fixedDeltaTime);
            //transform.rotation = Quaternion.Euler(0, rotY, 0);

            transform.rotation = newRotation;

            if (frame != lastFrameSaved)
            {
                ServerNetworkFrames.Instance.AddFrame(clientId, frame, transform.position, transform.localEulerAngles);
                lastFrameSaved = frame;
            }
        }
        else
        {
            Vector3 direction = (newPosition - transform.position);
            if (direction.magnitude > 1f)
                transform.Translate(direction.normalized * speed * Time.fixedDeltaTime);
            else
                transform.position = newPosition;      

            transform.rotation = newRotation;
        }
    }

    public void MovePlayer(PlayerTransform playerTransform)
    {
        if (playerTransform.hasPosition)
        {
            newPosition = new Vector3(playerTransform.xPos, playerTransform.yPos, playerTransform.zPos);
            Debug.Log(newPosition);
        }
        
        if (playerTransform.hasRotation)
        {

            newRotation = Quaternion.Euler(playerTransform.xRot, playerTransform.yRot, playerTransform.zRot);
        }

        this.frame = playerTransform.frame;
    }

    public void MovePlayer(PlayerInput playerInput)
    {
        this.horizontal = playerInput.horizontal;
        this.vertical = playerInput.vertical;
        this.camVertical = playerInput.camVertical;
        this.camHorizontal = playerInput.camHorizontal;

        newRotation = Quaternion.Euler(playerInput.xRot, playerInput.yRot, playerInput.zRot);
        weapon.Rotate(playerInput.xCamRot);

        this.frame = playerInput.frame;
    }

    public void Shoot()
    {
        weapon.Shoot();
    }

    public PlayerTransformPacket GetNewTransform()
    {
        PlayerTransformPacket packet = new PlayerTransformPacket();
        
        if (transform.position == lastPositionSent &&
            transform.eulerAngles == lastRotationSent &&
            transform.localScale == lastScaleSent)
            return null;

        packet.payload = new PlayerTransform();

        packet.payload.hasPosition = false;
        packet.payload.hasRotation = false;
        packet.payload.hasScale = false;

        if (transform.position != lastPositionSent)      
        {       
            packet.payload.hasPosition = true;
            packet.payload.xPos = transform.position.x;
            packet.payload.yPos = transform.position.y;
            packet.payload.zPos = transform.position.z;
            lastPositionSent = transform.position;
        } 

        if (transform.eulerAngles != lastRotationSent)      
        {       
            packet.payload.hasRotation = true;
            packet.payload.xRot = transform.eulerAngles.x;
            packet.payload.yRot = transform.eulerAngles.y;
            packet.payload.zRot = transform.eulerAngles.z;
            lastRotationSent = transform.eulerAngles;
        }

        if (transform.localScale != lastScaleSent)      
        {       
            packet.payload.hasScale = true;
            packet.payload.xSca = transform.localScale.x;
            packet.payload.ySca = transform.localScale.y;
            packet.payload.zSca = transform.localScale.z;
            lastScaleSent = transform.localScale;
        }

        packet.payload.frame = frame;

        return packet;
    }
}
