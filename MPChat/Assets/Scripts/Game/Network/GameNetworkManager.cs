﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.Net;
using System;

public class GameNetworkManager : MonoBehaviourSingleton<GameNetworkManager>
{
    const float SENDFRECUENCY = 0.01f;

    [HideInInspector]
    public bool isServer { get; set; }
    public uint objectId  { get; private set; }

    public GameObject localPlayerPrefab;
    public GameObject netPlayerPrefab;

    List<PlayerFrame> lastFrameRecieved = new List<PlayerFrame>();

    public LocalPlayerNet localPlayerNet;
    Dictionary<uint, NetworkPlayerNet> networkPlayersNet = new  Dictionary<uint, NetworkPlayerNet>();

    bool localSpawned = false;
    bool gameStarted = false;

    float counter = 0;
    
    void OnEnable()
    {
        objectId = NetworkManager.Instance.GetNewObjectId();
        PacketsManager.Instance.AddListener(objectId, OnRecieve);
    }

    void OnDisable()
    {
        PacketsManager.Instance.RemoveListener(objectId);
    }

    void Awake() 
    {
        ConnectionManager.Instance.AddClientConectedEvent(OnNewClient);
    }

    #region Update

    void Update()
    {
        if (!gameStarted)
            return;

        if (!ReadyToSend())
            return;

        //Server Functions
        //SendPlayersTransform();

        //Local Player Functions
        SendLocalPlayerInput();
        SendPlayersFrames();
    }

    void FixedUpdate()
    {
        if (!gameStarted)
            return;

        if (!ReadyToSend())
            return;

        //Server Functions
    }

    void LateUpdate()
    {
        if (!gameStarted)
            return;

        if (!ReadyToSend())
            return;

        //Server Functions
        SendPlayersTransform();
    }

    #endregion
    
    #region Network Events

    void OnRecieve(ushort type, Stream stream, IPEndPoint ip)
    {
        try
        {
            switch (type)
            {
                case (ushort)UserPacketType.NewPlayer:
                    SpawnPlayer(stream);
                    break;

                case  (ushort)UserPacketType.NewPlayerServerInfo:
                    UpdateToServerState(stream);
                    break;

                case (ushort)UserPacketType.PlayerInput:
                    MoveLocalPlayer(stream);
                    break;

                case (ushort)UserPacketType.PlayerTransform:
                    MovePlayer(stream);
                    break;

                case (ushort)UserPacketType.PlayerFrame:
                    CheckServerFrame(stream);
                    break;

                case (ushort)UserPacketType.PlayerEvent:
                    ProcessEvent(stream);
                    break;

                case (ushort)UserPacketType.TargetEvent:
                    ProcessTargetEvent(stream);
                    break;

                default:
                    break;
            }
        }
        catch (System.Exception ex)
        {
            Debug.LogError(ex.Message);
            throw ex;
        }
    }

    void OnNewClient(ulong id)
    {
        if (isServer)
        {
            uint newClientObjectId = NetworkManager.Instance.GetNewObjectId();
            NewPlayerPacket packet = new NewPlayerPacket();
            packet.payload = new NewPlayer(newClientObjectId, id);
            PacketsManager.Instance.SendPacket(packet, objectId, true);

            Debug.Log("Cliente conectado ObjectId: " + newClientObjectId);

            NewPlayerServerInfoPacket packetInfo = new NewPlayerServerInfoPacket();
            long timeStamp = DateTime.Now.ToBinary();
            List<int> points = new List<int>();
            List<ulong> clientIds = new List<ulong>();
            List<uint> objectIds = new List<uint>();
            List<Vector3> positions = new List<Vector3>();
            List<Vector3> rotations = new List<Vector3>();
            foreach (KeyValuePair<uint, NetworkPlayerNet> player in networkPlayersNet)
            {
                points.Add(GameManager.Instance.GetPlayerPoints(player.Value.clientId));
                clientIds.Add(player.Value.clientId);
                objectIds.Add(player.Key);
                positions.Add(player.Value.transform.position);
                rotations.Add(player.Value.transform.eulerAngles);
            }

            packetInfo.payload = new NewPlayerServerInfo(ServerNetworkFrames.Instance.GetCurrentFrame(), timeStamp, objectIds.Count, GameManager.Instance.GetActiveTarget(), points, clientIds, objectIds, positions, rotations);
            PacketsManager.Instance.SendPacket(packetInfo, id, objectId, true);

            SpawnPlayer(newClientObjectId, id);
        }
    }

    public void GameStarted()
    {
        gameStarted = true;
    }

    #endregion

    void SpawnPlayer (Stream stream)
    {   
        NewPlayerPacket packet = new NewPlayerPacket();
        packet.Deserialize(stream);

        if (packet.payload.clientId == ConnectionManager.Instance.localClientId)
        {
            if (!localSpawned)
            {
                GameObject player = Instantiate(localPlayerPrefab);
                LocalPlayerNet net = player.GetComponent<LocalPlayerNet>();
                net.SetObjectId(packet.payload.objectId);
                net.SetClientId(ConnectionManager.Instance.localClientId);
                localPlayerNet = net;
                Reconciliation.Instance.SetLocalPlayer(localPlayerNet);
            }   
        }
        else
        {
            GameObject player = Instantiate(netPlayerPrefab);
            NetworkPlayerNet net = player.GetComponent<NetworkPlayerNet>();
            net.SetObjectId(packet.payload.objectId);
            net.SetClientId(packet.payload.clientId);
            networkPlayersNet.Add(packet.payload.objectId, net);
            GameManager.Instance.AddPlayer(packet.payload.clientId);
        }
    }
  
    void SpawnPlayer (uint objectId, ulong clientId)
    {   
        GameObject player = Instantiate(netPlayerPrefab);
        NetworkPlayerNet net = player.GetComponent<NetworkPlayerNet>();
        net.SetObjectId(objectId);
        net.SetClientId(clientId);
        networkPlayersNet.Add(objectId, net);
        GameManager.Instance.AddPlayer(clientId);
    }

    void UpdateToServerState(Stream stream)
    {
        NewPlayerServerInfoPacket packet = new NewPlayerServerInfoPacket();
        packet.Deserialize(stream);
        ClientNetworkFrames.Instance.SetFrameNumber(packet.payload.currentFrame, DateTime.FromBinary(packet.payload.timeStamp));
        GameManager.Instance.SetActiveTarget(packet.payload.activeTarget);

        for (int i = 0; i < packet.payload.playersCount; i++)
        { 
            GameObject player = Instantiate(netPlayerPrefab, 
                                            packet.payload.positions[i], 
                                            new Quaternion(packet.payload.rotations[i].x, packet.payload.rotations[i].y, packet.payload.rotations[i].z, 0));
            NetworkPlayerNet net = player.GetComponent<NetworkPlayerNet>();
            net.SetObjectId(packet.payload.objectIds[i]);
            networkPlayersNet.Add(packet.payload.objectIds[i], net);
            GameManager.Instance.SetPlayerPoints(packet.payload.clientIds[i], packet.payload.points[i]);

        }
        GameManager.Instance.AddLocalPlayer();
    }

    void MoveLocalPlayer(Stream stream)
    {
        if (isServer)
        {
            PlayerInputPacket packet = new PlayerInputPacket();
            packet.Deserialize(stream);

            networkPlayersNet[packet.payload.objectId].MovePlayer(packet.payload);
        }
    }

    void MovePlayer(Stream stream)
    {
        PlayerTransformPacket packet = new PlayerTransformPacket();
        packet.Deserialize(stream);
        
        if (!isServer)
        {
            if (packet.payload.clientId != ConnectionManager.Instance.localClientId)
            {
                networkPlayersNet[packet.payload.objectId].MovePlayer(packet.payload);
            }
        }
        else 
        {
            networkPlayersNet[packet.payload.objectId].MovePlayer(packet.payload);
            PacketsManager.Instance.SendPacket(packet, objectId, false);
        }
    }

    void CheckServerFrame(Stream stream)
    {
        PlayerFramePacket packet = new PlayerFramePacket();
        packet.Deserialize(stream);

        Reconciliation.Instance.CheckServerFrame(packet.payload);
    }

    void ProcessEvent(Stream stream)
    {
        PlayerEventPacket packet = new PlayerEventPacket();

        packet.Deserialize(stream);

        switch (packet.payload.playerEvent)
        {
            case "Shoot":
                {
                    if (isServer)
                    {
                        networkPlayersNet[packet.payload.objectId].Shoot();
                        PacketsManager.Instance.SendPacket(packet, objectId, true);
                    }
                    else if (packet.payload.clientId != localPlayerNet.clientId)
                    {
                        networkPlayersNet[packet.payload.objectId].Shoot();
                    }
                }
                break;
            default:
                break;
        }
    }

    void ProcessTargetEvent(Stream stream)
    {
        TargetEventPacket packet = new TargetEventPacket();

        packet.Deserialize(stream);

        GameManager.Instance.ProcessTargetEvent(packet.payload.activate, packet.payload.target, packet.payload.playerId, packet.payload.score);
    }

    #region Metodos de envio

    public void SendPlayerEvent(string playerEvent, uint objectId, ulong clientId, uint frame)
    {
        if (isServer)
            return;

        PlayerEventPacket packet = new PlayerEventPacket();
        packet.payload = new PlayerEvent(playerEvent, clientId, objectId, frame);

        PacketsManager.Instance.SendPacket(packet, this.objectId, true);
    }

    public void SendTargetPacket(string tEvent, uint targetId, ulong playerId = 0, int score = 0)
    {
        TargetEventPacket packet = new TargetEventPacket();

        if (tEvent == "Activate")
            packet.payload = new TargetEvent(true, targetId, playerId, score);

        else
            packet.payload = new TargetEvent(false, targetId, playerId, score);

        PacketsManager.Instance.SendPacket(packet, objectId, true);
    }

    void SendLocalPlayerInput()
    {
        if (isServer || localPlayerNet == null)
            return;

        PlayerInputPacket packet = localPlayerNet.GetNewInput();

        if (packet == null)
            return;

        packet.payload.clientId = ConnectionManager.Instance.localClientId;
        packet.payload.objectId = localPlayerNet.objectId;
        packet.payload.frame = ClientNetworkFrames.Instance.GetFrameNumber();
        ClientNetworkFrames.Instance.AddFrame(packet.payload.horizontal, packet.payload.vertical, localPlayerNet.transform.position, localPlayerNet.transform.localEulerAngles);

        PacketsManager.Instance.SendPacket(packet, objectId, false);
    }
        
    void SendLocalPlayerTransform()
    {
        if (isServer || localPlayerNet == null)
            return;

        PlayerTransformPacket packet = localPlayerNet.GetNewTransform();

        if (packet == null)
            return;

        packet.payload.clientId = ConnectionManager.Instance.localClientId;
        packet.payload.objectId = localPlayerNet.objectId;

        PacketsManager.Instance.SendPacket(packet, objectId, false);
    }

    void SendPlayersTransform()
    {
        if (!isServer)
            return;

        foreach (KeyValuePair<uint, NetworkPlayerNet> player in networkPlayersNet)
	    {
            PlayerTransformPacket packet = player.Value.GetNewTransform();
            if (packet == null)
                continue;

            packet.payload.clientId = player.Value.clientId;
            packet.payload.objectId = player.Value.objectId;
            packet.payload.frame = player.Value.frame;

            PacketsManager.Instance.SendPacket(packet, objectId, false);
	    }
    }

    void SendPlayersFrames()
    {
        if (!isServer)
            return;

        foreach (KeyValuePair<uint, NetworkPlayerNet> player in networkPlayersNet)
        {
            PlayerFramePacket packet = new PlayerFramePacket();
            ServerFrame clientFrame = ServerNetworkFrames.Instance.GetCurrentFrameOfClient(player.Value.clientId);
           
            if (clientFrame != null)
            {
                packet.payload = new PlayerFrame(player.Value.clientId,
                                                 player.Value.objectId,
                                                 clientFrame.number,
                                                 clientFrame.position,
                                                 clientFrame.rotation);

                PacketsManager.Instance.SendPacket(packet, player.Value.clientId, this.objectId, false);
            }
        }
    }

    #endregion

    #region Metodos Auxiliares

    bool ReadyToSend()
    {
        counter += Time.deltaTime;
        if (counter < SENDFRECUENCY)
            return false;

        counter = 0;
        return true;
    }

    #endregion
}
