﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Reconciliation : Singleton<Reconciliation>
{
    float margin = 1;

    LocalPlayerNet localPlayer;

    protected override void Initialize()
    {
        base.Initialize();
    }

    public void CheckServerFrame(PlayerFrame frame)
    {
        uint frameServer;
        uint framePlayer;
        try
        {
            if (ClientNetworkFrames.Instance.localFrames.Count == 0)
                return;

            frameServer = frame.frame;
            framePlayer = ClientNetworkFrames.Instance.localFrames.Peek().number;
            if (frame.frame < ClientNetworkFrames.Instance.localFrames.Peek().number)
                return;

            while (frame.frame != ClientNetworkFrames.Instance.localFrames.Peek().number)
            {
                ClientNetworkFrames.Instance.localFrames.Dequeue();
            }

            Vector3 posDistance = ClientNetworkFrames.Instance.localFrames.Peek().position - frame.position;

            if (posDistance.magnitude > margin)
            {
                localPlayer.transform.position = frame.position;
                Debug.Log(ClientNetworkFrames.Instance.localFrames.Count);
                ClientNetworkFrames.Instance.localFrames.Dequeue();
                for (int i = 0; i < ClientNetworkFrames.Instance.localFrames.Count; i++)
                {
                    ClientFrame cFrame = ClientNetworkFrames.Instance.localFrames.Peek();
                    localPlayer.MovePlayer(cFrame.horizontal, cFrame.vertical);
                    ClientNetworkFrames.Instance.localFrames.Enqueue(new ClientFrame(cFrame.number, cFrame.horizontal, cFrame.vertical, localPlayer.transform.position, localPlayer.transform.localEulerAngles));
                    ClientNetworkFrames.Instance.localFrames.Dequeue();
                }
            }
        }
        catch (System.Exception ex)
        {
            throw;
        }
        
    }

    public void SetLocalPlayer(LocalPlayerNet localPlayer)
    {
        this.localPlayer = localPlayer;
    }
}
