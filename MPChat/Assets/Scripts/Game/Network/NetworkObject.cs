﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NetworkObject : MonoBehaviour
{
    public uint objectId  { get; private set; }
    public ulong clientId  { get; private set; }
    
    public void SetObjectId(uint objectId)
    {
        this.objectId = objectId;
    }

    public void SetClientId(ulong clientId)
    {
        this.clientId = clientId;
    }
}
