﻿using System.IO;
using UnityEngine;

public class PlayerTransform
{
    public ulong clientId;
    public uint objectId;
    public uint frame;

    public bool hasPosition;
    public float xPos;
    public float yPos;
    public float zPos;

    public bool hasRotation;
    public float xRot;
    public float yRot;
    public float zRot;

    public bool hasScale;
    public float xSca;
    public float ySca;
    public float zSca;

    public PlayerTransform ()
    {  
        hasPosition = false;
        hasRotation = false;
        hasScale = false;
    }    
}

public class PlayerInput
{
    public ulong clientId;
    public uint objectId;
    public uint frame;

    public float horizontal;
    public float vertical;

    public float camHorizontal;
    public float camVertical;
    public float xCamRot;
    
    public float xRot;
    public float yRot;
    public float zRot;

    public PlayerInput()
    {
    }

    public PlayerInput(ulong clientId, uint objectId, uint frame, float horizontal, float vertical, float camHorizontal, float camVertical, float xCamRot, float xRot, float yRot, float zRot)
    {
        this.clientId = clientId;
        this.objectId = objectId;
        this.frame = frame;
        this.horizontal = horizontal;
        this.vertical = vertical;
        this.camHorizontal = camHorizontal;
        this.camVertical = camVertical;
        this.xCamRot = xCamRot;
        this.xRot = xRot;
        this.yRot = yRot;
        this.zRot = zRot;
    }
}

public class PlayerTransformPacket : GameNetworkPacket<PlayerTransform>
{
    public PlayerTransformPacket() : base(UserPacketType.PlayerTransform)
    {
    }

    protected override void OnDeserialize(Stream stream)
    {
        BinaryReader br = new BinaryReader(stream);
        payload = new PlayerTransform();

        payload.clientId = br.ReadUInt64();
        payload.objectId = br.ReadUInt32();
        payload.frame = br.ReadUInt32();

        payload.hasPosition = br.ReadBoolean();
        if (payload.hasPosition)
        {
            payload.xPos = br.ReadSingle();
            payload.yPos = br.ReadSingle();
            payload.zPos = br.ReadSingle();
        }

        payload.hasRotation = br.ReadBoolean();
        if (payload.hasRotation)
        {
            payload.xRot = br.ReadSingle();
            payload.yRot = br.ReadSingle();
            payload.zRot = br.ReadSingle();
        }

        payload.hasScale = br.ReadBoolean();
        if (payload.hasScale)
        {
            payload.xSca = br.ReadSingle();
            payload.ySca = br.ReadSingle();
            payload.zSca = br.ReadSingle();
        }
    }

    protected override void OnSerialize(Stream stream)
    {
        BinaryWriter bw = new BinaryWriter(stream);

        bw.Write(payload.clientId);
        bw.Write(payload.objectId);
        bw.Write(payload.frame);


        bw.Write(payload.hasPosition);
        if (payload.hasPosition)
        {
            bw.Write(payload.xPos);
            bw.Write(payload.yPos);
            bw.Write(payload.zPos);
        }
            
        bw.Write(payload.hasRotation);
        if (payload.hasRotation)
        {
            bw.Write(payload.xRot);
            bw.Write(payload.yRot);
            bw.Write(payload.zRot);
        }

        bw.Write(payload.hasScale);
        if (payload.hasScale)
        {
            bw.Write(payload.xSca);
            bw.Write(payload.ySca);
            bw.Write(payload.zSca);
        }
    }
}

public class PlayerInputPacket : GameNetworkPacket<PlayerInput>
{
    public PlayerInputPacket() : base(UserPacketType.PlayerInput)
    {

    }

    protected override void OnDeserialize(Stream stream)
    {
        BinaryReader br = new BinaryReader(stream);

        ulong clientId = br.ReadUInt64();
        uint objectId = br.ReadUInt32();
        uint frame = br.ReadUInt32();

        float horizontal = br.ReadSingle();
        float vertical = br.ReadSingle();

        float camHorizontal = br.ReadSingle();
        float camVertical = br.ReadSingle();
        float xCamRot = br.ReadSingle();


        float xRot = br.ReadSingle();
        float yRot = br.ReadSingle();
        float zRot = br.ReadSingle();

        payload = new PlayerInput(clientId, objectId, frame, horizontal, vertical, camHorizontal, camVertical, xCamRot, xRot, yRot, zRot);    
    }

    protected override void OnSerialize(Stream stream)
    {
        BinaryWriter bw = new BinaryWriter(stream);

        bw.Write(payload.clientId);
        bw.Write(payload.objectId);
        bw.Write(payload.frame);

        bw.Write(payload.horizontal);
        bw.Write(payload.vertical);

        bw.Write(payload.camHorizontal);
        bw.Write(payload.camVertical);
        bw.Write(payload.xCamRot);

        bw.Write(payload.xRot);
        bw.Write(payload.yRot);
        bw.Write(payload.zRot);
    }
}
