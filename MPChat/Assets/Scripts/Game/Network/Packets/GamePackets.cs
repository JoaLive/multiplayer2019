using System.IO;
using System.Collections.Generic;
using UnityEngine;

public enum UserPacketType
{
    Message,
    PlayerTransform,
    PlayerInput,
    PlayerEvent,
    NewPlayer,
    NewPlayerServerInfo,
    PlayerFrame,
    TargetEvent
}

public interface GameNetPackets {}

#region Structs

public struct NewPlayer
{
    public uint objectId;
    public ulong clientId;

    public NewPlayer(uint objectId, ulong clientId)
    {
        this.objectId = objectId;
        this.clientId = clientId;
    }
}

public struct NewPlayerServerInfo
{
    public uint currentFrame;
    public long timeStamp;
    public int playersCount;
    public uint activeTarget;
    public List<int> points;
    public List<ulong> clientIds;
    public List<uint> objectIds;
    public List<Vector3> positions;
    public List<Vector3> rotations;

    public NewPlayerServerInfo(uint currentFrame, long timeStamp, int playersCount, uint activeTarget, List<int> points, List<ulong> clientIds, List<uint> objectIds, List<Vector3> positions, List<Vector3> rotations)
    {
        this.currentFrame = currentFrame;
        this.timeStamp = timeStamp;
        this.playersCount = playersCount;
        this.activeTarget = activeTarget;
        this.points = points;
        this.clientIds = clientIds;
        this.objectIds = objectIds;
        this.positions = positions;
        this.rotations = rotations;
    }
}

public struct PlayerFrame
{
    public ulong clientId;
    public uint objectId;
    public uint frame;
    public Vector3 position;
    public Vector3 rotation;

    public PlayerFrame(ulong clientId, uint objectId, uint frame, Vector3 position, Vector3 rotation)
    {
        this.clientId = clientId;
        this.objectId = objectId;
        this.frame = frame;
        this.position = position;
        this.rotation = rotation;
    }
}

public struct PlayerEvent
{
    public string playerEvent;
    public ulong clientId;
    public uint objectId;
    public uint frame;

    public PlayerEvent(string playerEvent, ulong clientId, uint objectId, uint frame)
    {
        this.playerEvent = playerEvent;
        this.clientId = clientId;
        this.objectId = objectId;
        this.frame = frame;
    }
}

public struct TargetEvent
{
    public bool activate;
    public uint target;
    public ulong playerId;
    public int score;

    public TargetEvent(bool activate, uint target, ulong playerId, int score)
    {
        this.activate = activate;
        this.target = target;
        this.playerId = playerId;
        this.score = score;
    }
}

#endregion

public abstract class GameNetworkPacket<T> : NetworkPacket<T>, GameNetPackets
{
    public GameNetworkPacket(UserPacketType type) : base((ushort)type, true)
    {
    }
}

public class StringMessagePacket : GameNetworkPacket<string>
{
    public StringMessagePacket() : base(UserPacketType.Message)
    {
    }

    protected override void OnDeserialize(Stream stream)
    {
        BinaryReader br = new BinaryReader(stream);
        payload = br.ReadString();
    }

    protected override void OnSerialize(Stream stream)
    {
        BinaryWriter bw = new BinaryWriter(stream);
        bw.Write(payload);
    }
}

public class ChatMessageMessagePacket : GameNetworkPacket<ChatMessage>
{
    public ChatMessageMessagePacket() : base (UserPacketType.Message)
    {
    }
    
    protected override void OnDeserialize(Stream stream)
    {
        BinaryReader br = new BinaryReader(stream);

        payload = new ChatMessage();
        payload.time = br.ReadString();
        payload.color = br.ReadString();
        payload.name = br.ReadString();
        payload.message = br.ReadString();
    }

    protected override void OnSerialize(Stream stream)
    {
        BinaryWriter bw = new BinaryWriter(stream);
        
        bw.Write(payload.time);
        bw.Write(payload.color);
        bw.Write(payload.name);
        bw.Write(payload.message);
    }
}

public class NewPlayerPacket : GameNetworkPacket<NewPlayer>
{
    public NewPlayerPacket() : base (UserPacketType.NewPlayer)
    {
    }
    
    protected override void OnDeserialize(Stream stream)
    {
        BinaryReader br = new BinaryReader(stream);

        payload = new NewPlayer(br.ReadUInt32(), br.ReadUInt64());
    }

    protected override void OnSerialize(Stream stream)
    {
        BinaryWriter bw = new BinaryWriter(stream);
        
        bw.Write(payload.objectId);
        bw.Write(payload.clientId);
    }
}

public class NewPlayerServerInfoPacket : GameNetworkPacket<NewPlayerServerInfo>
{
    public NewPlayerServerInfoPacket() : base(UserPacketType.NewPlayerServerInfo)
    {
    }

    protected override void OnDeserialize(Stream stream)
    {
        BinaryReader br = new BinaryReader(stream);

        List<int> points = new List<int>();
        List<ulong> clientIds = new List<ulong>();
        List<uint> objectIds = new List<uint>();
        List<Vector3> positions = new List<Vector3>();
        List<Vector3> rotations = new List<Vector3>();

        uint currentFrame = br.ReadUInt32();
        long timeStamp = br.ReadInt64();
        int playersCount = br.ReadInt32();
        uint activeTarget = br.ReadUInt32();

        for (int i = 0; i < playersCount; i++)
        {
            points.Add(br.ReadInt32());
            clientIds.Add(br.ReadUInt64());
            objectIds.Add(br.ReadUInt32());
            positions.Add(new Vector3(br.ReadSingle(), br.ReadSingle(), br.ReadSingle()));
            rotations.Add(new Vector3(br.ReadSingle(), br.ReadSingle(), br.ReadSingle()));
        }

        payload = new NewPlayerServerInfo(currentFrame, timeStamp, playersCount, activeTarget, points, clientIds, objectIds, positions, rotations);
    }

    protected override void OnSerialize(Stream stream)
    {
        BinaryWriter bw = new BinaryWriter(stream);

        bw.Write(payload.currentFrame);
        bw.Write(payload.timeStamp);
        bw.Write(payload.playersCount);
        bw.Write(payload.activeTarget);

        for (int i = 0; i < payload.playersCount; i++)
        {
            bw.Write(payload.points[i]);
            bw.Write(payload.clientIds[i]);
            bw.Write(payload.objectIds[i]);
            bw.Write(payload.positions[i].x);
            bw.Write(payload.positions[i].y);
            bw.Write(payload.positions[i].z);
            bw.Write(payload.rotations[i].x);
            bw.Write(payload.rotations[i].y);
            bw.Write(payload.rotations[i].z);
        }
    }
}

public class PlayerFramePacket : GameNetworkPacket<PlayerFrame>
{
    public PlayerFramePacket() : base(UserPacketType.PlayerFrame)
    {
    }

    protected override void OnDeserialize(Stream stream)
    {
        BinaryReader br = new BinaryReader(stream);

        ulong clientId = br.ReadUInt64();
        uint objecId = br.ReadUInt32();
        uint frame = br.ReadUInt32();

        Vector3 position = new Vector3(br.ReadSingle(), br.ReadSingle(), br.ReadSingle());
        Vector3 rotation = new Vector3(br.ReadSingle(), br.ReadSingle(), br.ReadSingle());

        payload = new PlayerFrame(clientId, objecId, frame, position, rotation);
    }

    protected override void OnSerialize(Stream stream)
    {
        BinaryWriter bw = new BinaryWriter(stream);

        bw.Write(payload.clientId);
        bw.Write(payload.objectId);
        bw.Write(payload.frame);

        bw.Write(payload.position.x);
        bw.Write(payload.position.y);
        bw.Write(payload.position.z);

        bw.Write(payload.rotation.x);
        bw.Write(payload.rotation.y);
        bw.Write(payload.rotation.z);
    }
}

public class PlayerEventPacket : GameNetworkPacket<PlayerEvent>
{
    public PlayerEventPacket() : base(UserPacketType.PlayerEvent)
    {
    }

    protected override void OnDeserialize(Stream stream)
    {
        BinaryReader br = new BinaryReader(stream);

        string playerEvent = br.ReadString();
        ulong clientId = br.ReadUInt64();
        uint objecId = br.ReadUInt32();
        uint frame = br.ReadUInt32();

        payload = new PlayerEvent(playerEvent, clientId, objecId, frame);
    }

    protected override void OnSerialize(Stream stream)
    {
        BinaryWriter bw = new BinaryWriter(stream);

        bw.Write(payload.playerEvent);
        bw.Write(payload.clientId);
        bw.Write(payload.objectId);
        bw.Write(payload.frame);
    }
}

public class TargetEventPacket : GameNetworkPacket<TargetEvent>
{
    public TargetEventPacket() : base(UserPacketType.TargetEvent)
    {
    }

    protected override void OnDeserialize(Stream stream)
    {
        BinaryReader br = new BinaryReader(stream);

        bool activate = br.ReadBoolean();
        uint target = br.ReadUInt32();
        ulong playerId = br.ReadUInt64();
        int score = br.ReadInt32();

        payload = new TargetEvent(activate, target, playerId, score);
    }

    protected override void OnSerialize(Stream stream)
    {
        BinaryWriter bw = new BinaryWriter(stream);

        bw.Write(payload.activate);
        bw.Write(payload.target);
        bw.Write(payload.playerId);
        bw.Write(payload.score);
    }
}