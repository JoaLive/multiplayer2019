﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Player
{
    public int number;
    public int score;

    public Player(int number, int score)
    {
        this.number = number;
        this.score = score;
    }

    public void AddScore()
    {
        this.score = this.score + 1;
    }

    public void SetScore(int score)
    {
        this.score = score;
    }
}

public class GameManager : MonoBehaviourSingleton<GameManager>
{
    Dictionary<ulong, Player> playersPoints = new Dictionary<ulong, Player>();
    GameObject[] targets;

    bool gameOver = false;
    uint activeTarget = 0;
    const int VICTORYSCORE = 10;
    public string localName;
    public Text score;
    public Text test;
    public Text victory;

    void Start()
    {
        targets = GameObject.FindGameObjectsWithTag("Target");

        SetActiveTarget((uint)Random.Range(0, targets.Length));

        //SetActiveTarget(0);
    }

    private void Update()
    {
        test.text = "activo: " + activeTarget;
    }

    private void UpdateScore()
    {
        if (gameOver == false)
        {
            string text = "";
            int i = 1;
            foreach (KeyValuePair<ulong, Player> player in playersPoints)
            {
                text += "Player " + player.Value.number + " Score: " + player.Value.score + System.Environment.NewLine;
                i++;
            }
            score.text = text;
            VictoryManager();
        }
        else
        {
           return;
        }
    }

    private void NextTarget(uint prevTarget)
    {
        if (!GameNetworkManager.Instance.isServer)
            return;

        Target next;
        do
        {
            next = targets[Random.Range(0, targets.Length)].GetComponent<Target>();
        } while (next.id == prevTarget);

        next.Activate();
        activeTarget = next.id;

        GameNetworkManager.Instance.SendTargetPacket("Activate", next.id);
    }

    public void AddPlayer(ulong playerId)
    {
        if (!playersPoints.ContainsKey(playerId))
        {
            playersPoints.Add(playerId, new Player(playersPoints.Count, 0));
            UpdateScore();
        }
    }

    public void AddLocalPlayer()
    {
        if (!playersPoints.ContainsKey(GameNetworkManager.Instance.localPlayerNet.clientId))
        {
            playersPoints.Add(GameNetworkManager.Instance.localPlayerNet.clientId, new Player(playersPoints.Count, 0));
            UpdateScore();
        }
    }

    public void TargetDown(ulong playerId, Target target)
    {
        if (GameNetworkManager.Instance.isServer)
        {
            playersPoints[playerId].AddScore();
            target.Deactivate();
            UpdateScore();
            GameNetworkManager.Instance.SendTargetPacket("Deactivate", target.id, playerId, playersPoints[playerId].score);


            NextTarget(target.id);
        }
    }

    public void ProcessTargetEvent(bool isActivationEvent, uint targetId, ulong playerId, int score)
    {
        if (isActivationEvent)
        {
            foreach (GameObject target in targets)
            {
                if (target.GetComponent<Target>().id == targetId)
                    target.GetComponent<Target>().Activate();
            }
            UpdateScore();
        }
        else
        {
            foreach (GameObject target in targets)
            {
                if (target.GetComponent<Target>().id == targetId)
                    target.GetComponent<Target>().Deactivate();
            }
            if (score > playersPoints[playerId].score)
                playersPoints[playerId].SetScore(score);
            UpdateScore();
        }
    }

    public void VictoryManager()
    {
        if (gameOver == false)
        {
            foreach (KeyValuePair<ulong, Player> player in playersPoints)
            {
                if (player.Value.score >= VICTORYSCORE)
                {
                    victory.text = "Player " + player.Value.number + " Wins";
                    gameOver = true;
                }
            }
        }
    }

    public int GetPlayerPoints(ulong playerId)
    {
        return playersPoints[playerId].score;
    }

    public void SetPlayerPoints(ulong playerId, int score)
    {
        if (!playersPoints.ContainsKey(playerId))
            playersPoints.Add(playerId, new Player(playersPoints.Count, score));
        else
            playersPoints[playerId].SetScore(score);

        UpdateScore();
    }

    public uint GetActiveTarget()
    {
        return activeTarget;
    }

    public void SetActiveTarget(uint active)
    {
        foreach (GameObject target in targets)
        {
            target.GetComponent<Target>().Deactivate();
        }

        activeTarget = active;
        targets[activeTarget].GetComponent<Target>().Activate();
    }
}
