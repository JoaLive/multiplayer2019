﻿using System.IO;
using System.Net;
using System.Collections.Generic;
using UnityEngine;

public class PacketsManager : Singleton<PacketsManager>, IReceiveData
{
    const int PACKETARRAYLENGHT = 1024;

    private Dictionary<uint, System.Action<ushort, Stream, IPEndPoint>> onPacketReceived = new Dictionary<uint, System.Action<ushort, Stream, IPEndPoint>>();

    private Dictionary <uint, uint[]> clientsRecievedPackets = new Dictionary<uint, uint[]>();
    private uint[] serverRecievedPackets = new uint[PACKETARRAYLENGHT];

    private uint currentPacketId = 0;
    private uint reliablePacketId = 0; // Agregado para que los packet reliable tengan su propia ID (Hollow)
    private uint previousPacketId = 0;

    protected override void Initialize()
    {
        base.Initialize();
        ConnectionManager.Instance.onReceiveEvent += OnReceiveData;
    }

    public void AddListener(uint ownerId, System.Action<ushort, Stream, IPEndPoint> callback)
    {
        if (!onPacketReceived.ContainsKey(ownerId))
            onPacketReceived.Add(ownerId, callback);
    }

    public void RemoveListener(uint ownerId)
    {
        if (onPacketReceived.ContainsKey(ownerId))
            onPacketReceived.Remove(ownerId);
    }

    public void SendPacket(ISerializablePacket packet, uint objectId, bool reliable = false)
    {
        byte[] bytes = Serialize(packet, objectId, reliable);

        if (NetworkManager.Instance.isServer)
            Broadcast(bytes, reliable);
        else
            PacketReliabilityManager.Instance.Send(bytes, reliable);
        if (reliable)
            reliablePacketId++; // Esto lo agregue para que el id de los reliable incremente (Hollow)
        currentPacketId++; // Esto lo agregue para que el id se incremente (Hollow)
    }

    public void SendPacket(ISerializablePacket packet, ulong clientId, uint objectId, bool reliable = false)
    {
        if (NetworkManager.Instance.isServer)
        {
            IPEndPoint iPEndPoint = ConnectionManager.Instance.GetIP(clientId);

            byte[] bytes = Serialize(packet, objectId, reliable, iPEndPoint);

            //Debug.Log($"Enviando " + bytes.Length + " Bytes.");

            PacketReliabilityManager.Instance.Send(bytes, iPEndPoint, reliable);
            currentPacketId++; // Esto lo agregue para que el id se incremente (Hollow)
        }
    }

    public void SendPacket(ISerializablePacket packet, IPEndPoint iPEndPoint, uint objectId, bool reliable = false)
    {
        if (NetworkManager.Instance.isServer)
        {
            byte[] bytes = Serialize(packet, objectId, reliable, iPEndPoint);

            //Debug.Log($"Enviando " + bytes.Length + " Bytes.");

            PacketReliabilityManager.Instance.Send(bytes, iPEndPoint, reliable);
            currentPacketId++; // Esto lo agregue para que el id se incremente (Hollow)
        }
    }

    public void Broadcast(byte[] data, bool reliable)
    {
        List<IPEndPoint> iPEndPoints = ConnectionManager.Instance.GetIPEndPoints();

        foreach (IPEndPoint ip in iPEndPoints)
        {
            PacketReliabilityManager.Instance.Send(data, ip, reliable);
        }
    }

    private byte[] Serialize(ISerializablePacket packet, uint objectId, bool reliable, IPEndPoint ipEndpoint = null)
    {
        PacketHeader header = new PacketHeader();
        MemoryStream stream = new MemoryStream();

        previousPacketId = currentPacketId;
        header.id = currentPacketId;
        header.senderId = ConnectionManager.Instance.clientId;
        if (reliable)
        {
            header.reliableId = reliablePacketId; // Para poder serializarle una id de packet reliable (Hollow)
        }
        else
        {
            header.reliableId = 0; // Sino lo dejamos en 0 para no comernos nulls (Hollow)
        }
        header.objectId = objectId;
        header.packetType = (ushort)packet.packetType;   
        header.isReliable = reliable;

        header.Serialize(stream);

        if (packet.packetType == (ushort)PacketType.User)
        {
            UserTypeHeader userHeader = new UserTypeHeader();
            userHeader.packetType = (ushort)packet.UserType;
            userHeader.Serialize(stream);
        } 

        packet.Serialize(stream);

        stream.Close();

        return stream.ToArray();
    }

    public void OnReceiveData(byte[] data, IPEndPoint ipEndpoint)
    {
        PacketHeader header = new PacketHeader();
        MemoryStream stream = new MemoryStream(data);

        header.Deserialize(stream);

        if (CheckIfAlreadyRecieved(header.senderId, header.id))
        {
            if (header.isReliable)
                PacketReliabilityManager.Instance.AddSecuence(ipEndpoint, header.id);
            
            if (header.packetType == (ushort)PacketType.User)
            {
                UserTypeHeader userHeader = new UserTypeHeader();
                userHeader.Deserialize(stream);

               InvokeCallback(header.objectId, userHeader.packetType, stream, null);
            }
            else
               InvokeCallback(header.objectId, header.packetType, stream, ipEndpoint);
        }

        stream.Close();
    }

    void InvokeCallback(uint objectId, ushort type, Stream stream, IPEndPoint ipEndpoint)
    {
        if (onPacketReceived.ContainsKey(objectId))
            onPacketReceived[objectId].Invoke(type, stream, ipEndpoint);
    }

    bool CheckIfAlreadyRecieved(uint senderId, uint id)
    {
        if (NetworkManager.Instance.isServer)
        {
            if (!clientsRecievedPackets.ContainsKey(senderId))
                clientsRecievedPackets.Add(senderId, new uint[PACKETARRAYLENGHT]);

            if (clientsRecievedPackets[senderId][id % PACKETARRAYLENGHT] == id)
                return false;
            else
            {
                clientsRecievedPackets[senderId][id % PACKETARRAYLENGHT] = id;
                return true;
            }
        }

        if (serverRecievedPackets[id % PACKETARRAYLENGHT] == id)
            return false;
        else
        {
            serverRecievedPackets[id % PACKETARRAYLENGHT] = id;
            return true;
        }
    }
}
