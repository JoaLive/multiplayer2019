﻿using System.IO;

public class PacketHeader : ISerializablePacket
{
    public uint senderId;
    public uint id;
    public uint reliableId; // Agregado para poder darle el numero a los packets reliable (Hollow)
    public uint objectId;
    public ushort packetType { get; set; }
    public ushort UserType { get; set; }
    public bool isReliable { get; set; }

    public void Serialize(Stream stream)
    {
        BinaryWriter bw = new BinaryWriter(stream);

        bw.Write(id);
        bw.Write(senderId);
        bw.Write(reliableId); // Agregado para poder darle el numero a los packets reliable (Hollow)
        bw.Write(objectId);
        bw.Write(packetType);
        bw.Write(UserType);
        bw.Write(isReliable);

        OnSerialize(stream);
    }

    public void Deserialize(Stream stream)
    {
        BinaryReader br = new BinaryReader(stream);

        id = br.ReadUInt32();
        senderId = br.ReadUInt32();
        reliableId = br.ReadUInt32(); // Agregado para poder darle el numero a los packets reliable (Hollow)
        objectId = br.ReadUInt32();
        packetType = br.ReadUInt16();
        UserType = br.ReadUInt16();
        isReliable = br.ReadBoolean();

        OnDeserialize(stream);
    }

    protected virtual void OnSerialize(Stream stream)
    {
    }

    protected virtual void OnDeserialize(Stream stream)
    {
    }
}

public class UserTypeHeader : ISerializablePacket
{
    public ushort packetType { get; set; }
    public ushort UserType { get; set; }

    public void Serialize(Stream stream)
    {
        BinaryWriter bw = new BinaryWriter(stream);

        bw.Write(packetType);

        OnSerialize(stream);
    }

    public void Deserialize(Stream stream)
    {
        BinaryReader br = new BinaryReader(stream);

        packetType = br.ReadUInt16();

        OnDeserialize(stream);
    }

    protected virtual void OnSerialize(Stream stream)
    {
    }

    protected virtual void OnDeserialize(Stream stream)
    {
    }
}
