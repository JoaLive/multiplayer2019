﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.Net;
using System.Linq;

public struct ReliablePacket
{
    public byte[] data;
    public uint ID;
    public uint RELIABLEID; // La reliable ID (Hollow)
    public IPEndPoint ipEndPoint;

    public ReliablePacket(byte[] data, uint ID, uint RELIABLEID, IPEndPoint ipEndPoint = null)
    {
        this.data = data;
        this.ID = ID;
        this.RELIABLEID = RELIABLEID; // La reliable ID (Hollow)
        this.ipEndPoint = ipEndPoint;
    }
}

public class PacketReliabilityManager : MonoBehaviourSingleton<PacketReliabilityManager>
{
    const int QUEUELENGTH = 1024;
    const int SENDFRECUENCY = 2;

    float sendCount = 0;
    Dictionary<IPEndPoint, uint[]> clientsSecuenceQueue;
    uint[] serverSecuenceQueue;

    Dictionary<uint, byte[]> serverUnprocessedPacketsList;
    List<uint> serverProcessedPacketsList;
    Dictionary<IPEndPoint, Dictionary<uint, byte[]>> clientUnprocessedPacketsList;
    Dictionary<IPEndPoint, List<uint>> clientProcessedPacketsList;

    List<ReliablePacket> packetsToSend;
    
    object handler = new object();

    protected override void Initialize()
    {
        base.Initialize();
    }

    void Awake()
    {
        packetsToSend = new List<ReliablePacket>();

        clientsSecuenceQueue = new Dictionary<IPEndPoint, uint[]>();
        serverSecuenceQueue = new uint[QUEUELENGTH];
        serverProcessedPacketsList = new List<uint>();
        serverUnprocessedPacketsList = new Dictionary<uint, byte[]>();
        clientProcessedPacketsList = new Dictionary<IPEndPoint, List<uint>>();
        clientUnprocessedPacketsList = new Dictionary<IPEndPoint, Dictionary<uint, byte[]>>();
    }

    void Update()
    {
        sendCount += Time.deltaTime;
        if (sendCount >= SENDFRECUENCY)
        {

                foreach (ReliablePacket packet in packetsToSend.ToArray())
                {
                    if (packet.ipEndPoint != null)
                        Send(packet.data, packet.ipEndPoint, true);
                    else
                        Send(packet.data, true);
                }
            sendCount = 0;
        }
    }

    private void AddPacketToSend(bool isReliable, byte[] bytes, IPEndPoint ipEndPoint = null)
    {
        MemoryStream stream = new MemoryStream(bytes);
        PacketHeader header = new PacketHeader();

        header.Deserialize(stream);

        foreach (ReliablePacket packet in packetsToSend)
        {
            if (packet.ID == header.id && packet.data == bytes)
                return;
        }

        if (isReliable)
            packetsToSend.Add(new ReliablePacket(bytes, header.id, header.reliableId, ipEndPoint)); // Modificado con reliable ID (Hollow)
        else
            packetsToSend.Add(new ReliablePacket(bytes, header.id, header.reliableId)); // Modificado con reliable ID (Hollow)

    }

    public void Send(byte[] bytes, IPEndPoint iPEndPoint, bool isReliable)
    {
        MemoryStream stream = new MemoryStream();
        Acknowledge ack = new Acknowledge();
        CRC32 theChecksum = new CRC32(); // para hacer el checksum (Hollow)

        ack.isReliable = isReliable;

        if (clientsSecuenceQueue.ContainsKey(iPEndPoint))
        {
            ack.secuenceQueue = clientsSecuenceQueue[iPEndPoint];
        }

        ack.checksumBytes = theChecksum.ComputeChecksumBytes(bytes); // Para hacer el checksum (Hollow)

        if (isReliable)
        {
            AddPacketToSend(isReliable, bytes, iPEndPoint);
        }

        ack.Serialize(stream);

        BinaryWriter bw = new BinaryWriter(stream);
        bw.Write(bytes.Length);
        bw.Write(bytes);

        NetworkManager.Instance.Send(stream.ToArray(), iPEndPoint);
    }

    public void Send(byte[] bytes, bool isReliable)
    {
        MemoryStream stream = new MemoryStream();
        Acknowledge ack = new Acknowledge();
        CRC32 theChecksum = new CRC32(); // Para hacer le checksum (Hollow)

        ack.isReliable = isReliable;
        ack.secuenceQueue = serverSecuenceQueue;
        ack.checksumBytes = theChecksum.ComputeChecksumBytes(bytes); // Para hacer el checksum (Hollow)

        if (isReliable)
        {
            AddPacketToSend(isReliable, bytes);
        }

        ack.Serialize(stream);

        BinaryWriter bw = new BinaryWriter(stream);
        bw.Write(bytes.Length);
        bw.Write(bytes);

        NetworkManager.Instance.SendToServer(stream.ToArray());
    }

    public void OnReceive(byte[] data, IPEndPoint ip)
    {
        Acknowledge ack = new Acknowledge();
        MemoryStream stream = new MemoryStream(data);
        CRC32 theChecksum = new CRC32();
        ack.Deserialize(stream);
        BinaryReader br = new BinaryReader(stream);
        byte[] newData = br.ReadBytes(br.ReadInt32());
        byte[] checksumProcess = theChecksum.ComputeChecksumBytes(newData);

        if (ack.isReliable)
        {
            if (!checksumProcess.SequenceEqual(ack.checksumBytes)) // Para comprobar que todos los numeros del checksum dan 0 (Hollow)
            {
                return;
            }
        }
 
        List<uint> secuences = new List<uint>();
        secuences = ack.ResolveAcknowledgeBits();
        ProcessSecuences(ack.sequence, secuences);
        if (ack.isReliable)
        {
            if (ProcessReliableID(ack.reliableSequence, ip))
            {
                ConnectionManager.Instance.OnReceiveData(newData, ip);
                if (NetworkManager.Instance.isServer)
                {
                    clientProcessedPacketsList[ip].Add(ack.reliableSequence);
                    foreach (KeyValuePair<uint, byte[]> item in clientUnprocessedPacketsList[ip])
                    {
                        if (ProcessReliableID(item.Key, ip))
                        {
                            ConnectionManager.Instance.OnReceiveData(item.Value, ip);
                            clientProcessedPacketsList[ip].Add(item.Key);
                            clientUnprocessedPacketsList[ip].Remove(item.Key);
                        }
                    }
                }
                else
                {
                    serverProcessedPacketsList.Add(ack.reliableSequence);
                    foreach (KeyValuePair<uint, byte[]> item in serverUnprocessedPacketsList)
                    {
                        if (ProcessReliableID(item.Key, ip))
                        {
                            ConnectionManager.Instance.OnReceiveData(item.Value, ip);
                            serverProcessedPacketsList.Add(item.Key);
                            serverUnprocessedPacketsList.Remove(item.Key);
                        }
                    }
                }
            }
            else
            {
                if (NetworkManager.Instance.isServer)
                {
                    clientUnprocessedPacketsList[ip].Add(ack.reliableSequence, newData);
                }
                else
                {
                    serverUnprocessedPacketsList.Add(ack.reliableSequence, newData);
                }
            }
        }
        else
        {
            ConnectionManager.Instance.OnReceiveData(newData, ip);
        }
    }

    public void AddSecuence(IPEndPoint ip, uint secuence)
    {
        if (NetworkManager.Instance.isServer)
        {
            if (!clientsSecuenceQueue.ContainsKey(ip))
                AddClient(ip);

            clientsSecuenceQueue[ip][secuence % QUEUELENGTH] = secuence;
        }
        else
        {
            serverSecuenceQueue[secuence % QUEUELENGTH] = secuence;
        }
    }

    public void AddClient(IPEndPoint ip)
    {
        clientsSecuenceQueue.Add(ip, new uint[QUEUELENGTH]);
        clientUnprocessedPacketsList.Add(ip, new Dictionary<uint, byte[]>());
        clientProcessedPacketsList.Add(ip, new List<uint>());
    }

    public void ProcessSecuences(uint LastSecuence, List<uint> secuences)
    {        
        foreach (ReliablePacket packet in packetsToSend.ToArray())
        {
            if (packet.ID == LastSecuence)
                packetsToSend.Remove(packet);
                
            foreach (uint secuence in secuences)
            {
                if (packet.ID == secuence || packet.ID == LastSecuence)
                    packetsToSend.Remove(packet);
            }
        }
    }

    private bool ProcessReliableID(uint ReliableId, IPEndPoint ip = null)
    {
        if (ReliableId == 0)
        {
            return true;
        }
        if (!NetworkManager.Instance.isServer)
        {
            if (serverProcessedPacketsList.Contains(ReliableId - 1))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        if (clientProcessedPacketsList[ip].Contains(ReliableId - 1))
        {
            return true;
        }
        else
        {
            return false;
        }
    }
}
