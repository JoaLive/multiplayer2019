﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class Acknowledge
{
    public uint[] secuenceQueue { get; set; }
    public uint sequence { get; set; }
    public uint reliableSequence { get; set; }
    public uint acknowledge { get; set; }
    public int acknowledge_bits { get; set; }
    public bool isReliable { get; set; }
    public byte[] checksumBytes { get; set; }
    // CRC32 Checksum (Mirar como hacerlo)

    public void Serialize(Stream stream)
    {
        BinaryWriter bw = new BinaryWriter(stream);

        if (secuenceQueue != null)
        {
            bw.Write(true);
            GetLastSecuence();
            CreateAcknowledgeBits();

            bw.Write(sequence);
            bw.Write(reliableSequence);
            //bw.Write(acknowledge);
            bw.Write(acknowledge_bits);
            bw.Write(isReliable);
            bw.Write(checksumBytes.Length);
            bw.Write(checksumBytes);
        }
        else
            bw.Write(false);

        OnSerialize(stream);
    }

    public void Deserialize(Stream stream)
    {
        BinaryReader br = new BinaryReader(stream);
        bool hasData = br.ReadBoolean();

        if (hasData)
        {
            sequence = br.ReadUInt32();
            reliableSequence = br.ReadUInt32();
            //acknowledge = br.ReadUInt32();
            acknowledge_bits = br.ReadInt32();
            isReliable = br.ReadBoolean();
            int checksumLength = br.ReadInt32();
            checksumBytes = br.ReadBytes(checksumLength);
        }

        OnDeserialize(stream);
    }

    public List<uint> ResolveAcknowledgeBits()
    {
        List<uint> secuences = new List<uint>();

        for (uint i = 0; i < 32; i++)
        {
            if ((acknowledge_bits & (1 << (int)i)) != 0 )
            {
                secuences.Add(sequence - (i + 1));
            }
        }

        return secuences;
    }

    protected virtual void OnSerialize(Stream stream)
    {
    }

    protected virtual void OnDeserialize(Stream stream)
    {
    }

    private void CreateAcknowledgeBits()
    {
        acknowledge_bits = 0;
        for (int i = 0; i < 32; i++)
        {
            if (secuenceQueue[i] != sequence && secuenceQueue[i] != 0)
            {
                uint diff = sequence - secuenceQueue[i];
                acknowledge_bits = turnOnK(acknowledge_bits, (int)diff); 
            }
        }
    }

    private void GetLastSecuence()
    {
        uint last = 0;

        foreach (uint item in secuenceQueue)
        {
            if (item > last)
                last = item;
        }

        sequence = last;
    }

    private int turnOnK(int n, int k) 
    { 
        if (k <= 0) 
            return n; 
      
        return (n | (1 << (k - 1))); 
    } 
}
