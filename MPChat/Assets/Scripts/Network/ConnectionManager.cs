﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Net;
using System;
using System.IO;

public struct Client
{
    public float lastMsgTimeStamp;
    public ulong id;
    public IPEndPoint ipEndPoint;

    public Client(IPEndPoint ipEndPoint, ulong id, float timeStamp)
    {
        this.lastMsgTimeStamp = timeStamp;
        this.id = id;
        this.ipEndPoint = ipEndPoint;
    }
}

public struct NewClient
{
    public float lastMsgTimeStamp;
    public ulong id;
    public IPEndPoint ipEndPoint;
    public ulong clientSalt;
    public ulong serverSalt;

    public NewClient(IPEndPoint ipEndPoint, ulong id, float timeStamp, ulong clientSalt, ulong serverSalt)
    {
        this.lastMsgTimeStamp = timeStamp;
        this.id = id;
        this.ipEndPoint = ipEndPoint;
        this.clientSalt = clientSalt;
        this.serverSalt = serverSalt;
    }
}

public class ConnectionManager : MonoBehaviourSingleton<ConnectionManager>, IReceiveData
{
    public int timeOut = 30;

    public uint clientId
    {
        get; private set;
    }

    public uint newClientId
    {
        get; private set;
    }
    
    public ulong localClientId
    {
        get; private set;
    }

    private readonly Dictionary<ulong, Client> clients = new Dictionary<ulong, Client>();
    private readonly Dictionary<IPEndPoint, ulong> ipToId = new Dictionary<IPEndPoint, ulong>();
    
    private readonly Dictionary<ulong, NewClient> newClients = new Dictionary<ulong, NewClient>();
    private readonly Dictionary<IPEndPoint, ulong> newIpToId = new Dictionary<IPEndPoint, ulong>();

    private readonly List<ulong> clientIds = new List<ulong>();

    public Action<byte[], IPEndPoint> onReceiveEvent;
    public List<Action<ulong>> onClientConectedEvents = new List<Action<ulong>>();

    private float sendDelay = 0.02f;
    private float counter = 0;
    private bool isServer;
    private uint objectId;

    private bool onConectionRequest = false;
    private bool onChallengeResponse = false;
    private bool connected = false;
    
    private ulong clientSalt;
    private ulong serverSalt;
    private ulong challengeResolved;

    private void Update() 
    {
        //Debug.Log(clients.Count);

        counter += Time.deltaTime;
        if (counter < sendDelay)
            return;

        counter -= counter;

        if (newClients.Count > 0)
        {
            using (var iterator = newClients.GetEnumerator())
            {
                while (iterator.MoveNext())
                {
                    SendChallengeRequest(iterator.Current.Value.ipEndPoint, iterator.Current.Value.id, iterator.Current.Value.serverSalt);
                }
            }
        }
        else
        {
            if (onConectionRequest)
                SendConnectionRequest();
            
            if (onChallengeResponse)
                SendChallengeResponse();
        }
    }

    void OnEnable()
    {
        objectId = NetworkManager.Instance.GetNewObjectId();
        PacketsManager.Instance.AddListener(objectId, OnRecieve);
    }

    void OnDisable() 
    {
        PacketsManager.Instance.RemoveListener(objectId);
    }

    public void OnReceiveData(byte[] data, IPEndPoint ip)
    {
        if (onReceiveEvent != null)
            onReceiveEvent.Invoke(data, ip);
    }

    public void AddClient(IPEndPoint ip)
    {
        if (!ipToId.ContainsKey(ip))
        {
            //Debug.Log("Adding client: " + ip.Address);

            ipToId[ip] = newClients[newIpToId[ip]].id;

            clients.Add(newClients[newIpToId[ip]].id, new Client(ip, newClients[newIpToId[ip]].id, Time.realtimeSinceStartup));
            
            //Debug.Log("Cliente añadido: " + newClients[newIpToId[ip]].id);

            foreach (Action<ulong> action in onClientConectedEvents)
                action.Invoke(newClients[newIpToId[ip]].id);
                
            RemoveNewClient(ip);
        }
    }

    public void AddNewClient(IPEndPoint ip, ulong clientSalt)
    {
        if (!newIpToId.ContainsKey(ip) && !ipToId.ContainsKey(ip))
        {
            //Debug.Log("Adding new client: " + ip.Address);

            ulong id = NextClientId();
            newIpToId[ip] = id;

            ulong newServerSalt = NextUInt64(new System.Random());

            newClients.Add(id, new NewClient(ip, id, Time.realtimeSinceStartup, clientSalt, newServerSalt));
        }
    }

    void RemoveClient(IPEndPoint ip)
    {
        if (ipToId.ContainsKey(ip))
        {
            Debug.Log("Removing client: " + ip.Address);
            clients.Remove(ipToId[ip]);
        }
    }

    void RemoveNewClient(IPEndPoint ip)
    {
        if (newIpToId.ContainsKey(ip))
        {
            //Debug.Log("Removing client: " + ip.Address);
            newClients.Remove(newIpToId[ip]);
            newIpToId.Remove(ip);
        }
    }

    void OnRecieve(ushort type, Stream stream, IPEndPoint ip)
    {
        switch(type)
        {
            case (ushort)PacketType.ConnectionRequest:
            {
                    Debug.Log("Recibi ConectionRequest");
                ConnectionRequestPacket packet = new ConnectionRequestPacket();
                packet.Deserialize(stream);
                AddNewClient(ip, packet.payload);
                break;
            }
            
            case (ushort)PacketType.ChallengeRequest:
                {
                    Debug.Log("Recibi ChallengeRequest");
                    onConectionRequest = false;
                ChallengeRequestPacket packet = new ChallengeRequestPacket();
                packet.Deserialize(stream);
                serverSalt = packet.payload;
                localClientId = packet.clientId;

                DoChallenge();
                break;
            }
            
            case (ushort)PacketType.ChallengeResponse:
                {
                    Debug.Log("Recibi ChallengeResponse");
                    ChallengeResponsePacket packet = new ChallengeResponsePacket();
                packet.Deserialize(stream);
                CheckChallengeRecieved(ip, packet.payload);
                break;
            }        
            
            case (ushort)PacketType.Conected:
                Debug.Log("Recibi ChallengeResponse");
                OnConected();
                break;
        }
    }

    void SendConnectionRequest() 
    {   
        Debug.Log("Enviando Connection Request");
        ConnectionRequestPacket packet = new ConnectionRequestPacket();
        packet.payload = clientSalt;
        PacketsManager.Instance.SendPacket(packet, objectId);
    }

    void SendChallengeRequest(IPEndPoint ip, ulong id, ulong serverSalt) 
    {   
        Debug.Log("Enviando ChallengeRequest");
        ChallengeRequestPacket packet = new ChallengeRequestPacket();
        packet.payload = serverSalt;
        packet.clientId = id;
        PacketsManager.Instance.SendPacket(packet, ip, objectId);
    }

    void SendChallengeResponse() 
    {
        Debug.Log("Enviando Challenge Response");
        ChallengeResponsePacket packet = new ChallengeResponsePacket();
        packet.payload = challengeResolved;
        PacketsManager.Instance.SendPacket(packet, objectId);
    }

    void SendConected(IPEndPoint ip) 
    {
        Debug.Log("Enviando Conected");

        ConectedPacket packet = new ConectedPacket();
        packet.payload = "test";
        PacketsManager.Instance.SendPacket(packet, ip, objectId);
    }

    void OnConected()
    {
        Debug.Log("Conected to Server");
        onChallengeResponse = false;
        connected = true;
    }

    void DoChallenge()
    {
        challengeResolved = serverSalt ^ clientSalt;
        onChallengeResponse = true;
    }
    
    void CheckChallengeRecieved(IPEndPoint ip, ulong answerRecieved)
    {
        if (newIpToId.ContainsKey(ip))
        {
            ulong answer = newClients[newIpToId[ip]].serverSalt ^ newClients[newIpToId[ip]].clientSalt;

            if (answer != answerRecieved)
                return;

            onChallengeResponse = false;
            
            AddClient(ip);
            PacketReliabilityManager.Instance.AddClient(ip);
            SendConected(ip);
        }
    }

    ulong NextClientId() 
    {
        bool idFound = false;
        ulong newId = 0;
        while (!idFound)
        {
            newId = NextUInt64(new System.Random());
            idFound = true;

            foreach (ulong id in clientIds)
            {
                if (id == newId)
                    idFound = false;
            }
        }       
        return newId;
    }

    public void ConnectToServer()
    {
        if (!connected && !onChallengeResponse)
        {
            onConectionRequest = true;

            clientSalt = NextUInt64(new System.Random());            
        }
    }

    public List<IPEndPoint> GetIPEndPoints()
    {
        List<IPEndPoint> iPEndPoints = new List<IPEndPoint>();
        using (var iterator = clients.GetEnumerator())
        {
            while (iterator.MoveNext())
            {
                iPEndPoints.Add(iterator.Current.Value.ipEndPoint);
            }
        }

        return iPEndPoints;
    }

    public IPEndPoint GetIP(ulong id)
    {
        return clients[id].ipEndPoint;
    }

    public void AddClientConectedEvent(Action<ulong> action)
    {
        onClientConectedEvents.Add(action);
    }

    private UInt64 NextUInt64(System.Random rnd)
    {
        var buffer = new byte[sizeof(UInt64)];
        rnd.NextBytes(buffer);
        return BitConverter.ToUInt64(buffer, 0);
    }
}
